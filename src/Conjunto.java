
public abstract class Conjunto implements Vendible{
	
	private Remera remera;
	
	private Prenda prenda;
	
	public Conjunto(Remera remera, Prenda prenda) {
		this.prenda = prenda;
		this.remera = remera;
	}
	
	@Override
	public abstract Double calcularPrecio();
	
	@Override
	public Double calcularPrecio(Tarjeta tarjeta) {
		return tarjeta.aplicarDescuento(this.calcularPrecio());		
	}
	
	
	public Remera getRemera() {
		return this.remera;
	}
	
	public Prenda getPrenda() {
		return this.prenda;
	}

}
