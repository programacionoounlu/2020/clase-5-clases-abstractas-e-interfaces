
public class ConjutnoDosPorUno extends Conjunto{
	
	private Prenda prendaElegida;
	
	public ConjutnoDosPorUno(Remera remera, Prenda prenda, Double montoDeDescuento) {
		super(remera, prenda);	
		this.usarPrecioDePrenda();
	}
	
	@Override
	public Double calcularPrecio() {		
		return this.prendaElegida.calcularPrecio();
	}
	
	public void usarPrecioDeRemera() {
		this.prendaElegida = this.getRemera();
	}
	
	public void usarPrecioDePrenda() {
		this.prendaElegida = this.getPrenda();
	}

}
