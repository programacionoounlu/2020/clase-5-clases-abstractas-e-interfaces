
public class ConjuntoConDescuento extends Conjunto{
	
	private Double montoDeDescuento;
	
	public ConjuntoConDescuento(Remera remera, Prenda prenda, Double montoDeDescuento) {
		super(remera, prenda);
		this.setMontoDeDescuento(montoDeDescuento);
	}

	@Override
	public Double calcularPrecio() { 
		return this.getPrenda().calcularPrecio() + 
				this.getRemera().calcularPrecio() - 
				this.getMontoDeDescuento();
	}
	
	public Double getMontoDeDescuento() {
		return this.montoDeDescuento;
	}
	
	public void setMontoDeDescuento(Double monto) {
		if(monto > 0) {
			this.montoDeDescuento = monto;
		}else {
			this.montoDeDescuento =  0.0;
		}
	}



}
