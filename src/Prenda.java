
public class Prenda implements Vendible{
	private Double precioDeLista;
	
	public Prenda(Double precioDeLista) {
		this.setPrecioDeLista(precioDeLista);		
	}
	
	public Double getPrecioDeLista() {
		return this.precioDeLista;
	}
	
	public void setPrecioDeLista(Double precioDeLista) {
		if(precioDeLista > 0.00) {
			this.precioDeLista = precioDeLista;
		}
	}
	
	public Double calcularPrecio() {
		return this.getPrecioDeLista() * 1.10;
	}
	
	public Double calcularPrecio(Tarjeta tarjeta) {		
		return tarjeta.aplicarDescuento(this.calcularPrecio());
	}

}
