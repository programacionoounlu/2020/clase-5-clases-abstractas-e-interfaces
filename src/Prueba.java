import java.util.ArrayList;
import java.util.List;

public class Prueba {

	public static void main(String[] args) {
		Prenda prenda = new Prenda(100.00);		
		Remera remera = new Remera(100.00);
		Sweater sweater = new Sweater(100.00);
		Camisa camisa = new Camisa(100.00, true);
		Tarjeta tarjeta = new Tarjeta();
		Conjunto conjConDto = new ConjuntoConDescuento(remera, camisa, 5.0);
//		System.out.println("Precio de prenda $" + prenda.calcularPrecio());
//		System.out.println("Precio de remera $" + remera.calcularPrecio());
		ArrayList<Vendible> listado = new ArrayList<Vendible>();
		listado.add(camisa);
		listado.add(remera);
		listado.add(sweater);			
		listado.add(prenda);
		listado.add(conjConDto);
		for (Vendible elemento : listado) {
			System.out.println("Precio del elemento $ " + elemento.calcularPrecio());
			System.out.println("Precio del elemento (con tarjeta) $ " + elemento.calcularPrecio(tarjeta));
			if(elemento instanceof Camisa) {
				Camisa camisa2 = (Camisa) elemento;
				if(camisa2.isMangaLarga()) {
					System.out.println("Es una camisa manga larga");
				}
			} else {
				System.out.println("Es otra elemento");
			}
		}
	}

}
