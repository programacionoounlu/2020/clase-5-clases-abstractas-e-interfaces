
public interface Vendible {
	public Double calcularPrecio();
	
	public Double calcularPrecio(Tarjeta tarjeta);
}
